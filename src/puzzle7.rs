// "one, two, three" = vec!["one", "two" "three"]

const PARSE1: &'static str = "pbga (66)";
const PARSE2: &'static str = "fwft (72) -> ktlj, cntj, xhth";

pub fn solve() {

 //   PARSE1.split_whitespace()

    // Search for node which noone depends on
    let node_list = vec![
        Node::new("pbga", 66, vec![]),
        Node::new("xhth", 57, vec![]),
        Node::new("ebii", 61, vec![]),
        Node::new("havc", 66, vec![]),
        Node::new("ktlj", 57, vec![]),
        Node::new("fwft", 72, vec!["ktlj", "cntj", "xhth"]),
        Node::new("qoyq", 66, vec![]),
        Node::new("padx", 45, vec!["pbga", "havc", "qoyq"]),
        Node::new("tknk", 41, vec!["ugml", "padx", "fwft"]),
        Node::new("jptl", 61, vec![]),
        Node::new("ugml", 68, vec!["gyxo", "ebii", "jptl"]),
        Node::new("gyxo", 61, vec![]),
        Node::new("cntj", 57, vec![])
    ];

    let non_leafs: Vec<Node> = node_list
        .iter()
        .cloned()
        .filter(|n| !n.children.is_empty())
        .collect();

    let root = non_leafs
        .iter()
        .find(|n| {
            non_leafs.iter().all(|m| m.children.iter().find(|&c| c == &n.name).is_none())
        })
        .unwrap();

    println!("{:?}", root);

}

#[derive(Clone, Debug)]
struct Node {
    name: &'static str,
    weight: u32,
    children: Vec<&'static str>,
}

impl Node {
    fn new(name: &'static str, weight: u32, children: Vec<&'static str>) -> Node {
        Node { name, weight, children }
    }
}
