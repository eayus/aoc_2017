use std::str::FromStr;

const JUMP_LIST: &'static str = include_str!("../jump_list");

pub fn solve() {
    let mut js = JUMP_LIST
        .lines()
        .map(|x| i32::from_str(x).unwrap())
        .collect::<Vec<i32>>();
    let a1 = part1::num_jumps(js.clone());
    let a2 = part2::num_jumps(js);
    println!("{} {}", a1, a2);
}

mod part1 {
    pub fn num_jumps(mut js: Vec<i32>) -> u32 {
        let mut ptr: usize = 0;
        let mut count: u32 = 0;

        loop {
            match js.get(ptr) {
                Some(&x) => {
                    js[ptr] += 1;
                    if x >= 0 {
                        ptr += x as usize;
                    } else {
                        ptr -= -x as usize;
                    }
                }
                None => return count,
            };
            count += 1;
        }
    }


    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn examples() {
            assert_eq!(num_jumps(vec![0, 3, 0, 1, -3]), 5);
        }
    }
}

mod part2 {
    pub fn num_jumps(mut js: Vec<i32>) -> u32 {
        let mut ptr: usize = 0;
        let mut count: u32 = 0;

        loop {
            match js.get(ptr) {
                Some(&x) => {
                    js[ptr] += match x {
                        x if x >= 3 => -1,
                        _ => 1,
                    };
                    if x >= 0 {
                        ptr += x as usize;
                    } else {
                        ptr -= -x as usize;
                    }
                }
                None => return count,
            };
            count += 1;
        }
    }


    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn examples() {
            assert_eq!(num_jumps(vec![0, 3, 0, 1, -3]), 10);
        }
    }

}

