use crate::itertools::Itertools;

pub fn solve() {
    let mut sets: Vec<Vec<u16>> = vec![vec![ 10, 3, 15, 10, 5, 15, 5, 15, 9, 2, 5, 8, 5, 2, 3, 6 ]];
    //let mut sets: Vec<Vec<u16>> = vec![vec![0,2,7,0]];

    let a1 = loop {
        let new_set = next_set(sets.last().unwrap());
        if sets.iter().find(|x| **x == new_set).is_some() {
            break sets.len();
        }
        sets.push(new_set);
    };

    sets = vec![vec![ 10, 3, 15, 10, 5, 15, 5, 15, 9, 2, 5, 8, 5, 2, 3, 6 ]];
    //sets = vec![vec![0,2,7,0]];


    let a2 = loop {
        let new_set = next_set(sets.last().unwrap());
        if let Some(i) = sets.iter().position(|x| *x == new_set) {
            break sets.len() - i;
        }
        sets.push(new_set);
    };


    println!("{} {}", a1, a2);
}

fn next_set(current: &Vec<u16>) -> Vec<u16> {
        let mut new = current.clone();

        let (max_index, &spare_count) = new
            .iter()
            .enumerate()
            .rev()
            .max_by_key(|(_, &n)| n)
            .unwrap();

        new[max_index as usize] = 0;

        (0..new.len())
            .cycle()
            .skip(max_index as usize + 1)
            .take(spare_count as usize)
            .for_each(|i| new[i] += 1);

        new
}
