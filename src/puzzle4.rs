const PASSPHRASE_LIST: &'static str = include_str!("../passphrase_list");
use itertools::Itertools;

pub fn solve() {
    let a1 = PASSPHRASE_LIST
        .lines()
        .filter(|pass| {
            let words = pass.split_whitespace();
            words.clone().unique().count() == words.count()
        })
        .count();

    let a2 = PASSPHRASE_LIST
        .lines()
        .filter(|pass| {
            let words = pass.split_whitespace().map(|word| {
                let mut w = word.chars().collect::<Vec<char>>();
                w.sort_unstable();
                w
            });
            words.clone().unique().count() == words.count()
        })
        .count();



    println!("{}, {}", a1, a2);
}
