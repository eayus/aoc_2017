#[macro_use]
extern crate nom;
extern crate itertools;

mod puzzle1;
mod puzzle2;
mod puzzle3;
mod puzzle4;
mod puzzle5;
mod puzzle6;
mod puzzle7;

fn main() {
    puzzle1::solve();
    puzzle2::solve();
    puzzle3::solve();
    puzzle4::solve();
    puzzle5::solve();
    puzzle6::solve();
    puzzle7::solve();
}
