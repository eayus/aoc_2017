use crate::itertools::iterate;
use std::ops::Add;

const INPUT_SQUARE: usize = 361527;

pub fn solve() {
    let a1 = part1::steps(INPUT_SQUARE);
    let a2 = part2::first_larger(INPUT_SQUARE);
    println!("{}, {}", a1, a2);
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
struct GridPosition {
    pub x: i32,
    pub y: i32,
}

impl GridPosition {
    fn first() -> Self {
        GridPosition {
            x: 0,
            y: 0,
        }
    }

    fn next(&self) -> Self {
        let ring_num = self.ring_num();

        let offset = match (self.x, self.y) {
            (x, y) if y == ring_num && x != -ring_num => (-1, 0),
            (x, y) if x == -ring_num && y != -ring_num => (0, -1),
            (x, y) if y == -ring_num => (1, 0),
            (x, y) if x == ring_num && y != ring_num => (0, 1),
            _ => (1, 0),
        };

        *self + offset
    }

    fn ring_num(&self) -> i32 {
        self.x.abs().max(self.y.abs())
    }

    fn center_distance(&self) -> i32 {
        self.x.abs() + self.y.abs()
    }

    fn adjacents(&self) -> [Self; 8] {
        [
            *self + (0, 1),
            *self + (1, 1),
            *self + (1, 0),
            *self + (1, -1),
            *self + (0, -1),
            *self + (-1, -1),
            *self + (-1, 0),
            *self + (-1, 1)
        ]
    }
}

impl From<(i32, i32)> for GridPosition {
    fn from((x, y): (i32, i32)) -> GridPosition {
        GridPosition { x, y }
    }
}

impl<T: Into<Self>> Add<T> for GridPosition {
        
    type Output = Self;

    fn add(self, other: T) -> Self {
        let other = other.into();
        GridPosition {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

/*
#[derive(Debug, Clone)]
struct GridPosition {
    pos: (i32, i32),
    ring_no: i32, // Always positive, i32 for convenience of calculations
}

impl GridPosition {
    fn first() -> Self {
        GridPosition {
            pos: (0, 0),
            ring_no: 0,
        }
    }

    fn next(&self) -> Self {
        let mut new = self.clone();

        /*if new.pos == (new.ring_no, -new.ring_no) {
            new.ring_no += 1;
        }*/

        new.ring_no = self.pos.0.abs().max(self.pos.1.abs());

        let change = match new.pos {
            (x, y) if y == new.ring_no && x != -new.ring_no => (-1, 0),
            (x, y) if x == -new.ring_no && y != -new.ring_no => (0, -1),
            (x, y) if y == -new.ring_no /*&& x != new.ring_no*/ => (1, 0),
            (x, y) if x == new.ring_no && y != new.ring_no => (0, 1),
            _ => (1, 0),
        };

        new.pos.0 += change.0;
        new.pos.1 += change.1;

        new
    }

    fn distance(&self) -> i32 {
        self.pos.0.abs() + self.pos.1.abs()
    }
}*/

mod part1 {
    use super::GridPosition;

    pub fn steps(n: usize) -> i32 {
        crate::itertools::iterate(GridPosition::first(), GridPosition::next).nth(n - 1).unwrap().center_distance()
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn examples() {
            assert_eq!(steps(1), 0);
            assert_eq!(steps(12), 3);
            assert_eq!(steps(23), 2);
            assert_eq!(steps(1024), 31);
        }
    }
}

mod part2 {
    use std::collections::HashMap;
    use super::GridPosition;

    pub fn first_larger(n: usize) -> usize {
        let mut hm: HashMap<GridPosition, usize> = HashMap::new();
        let mut pos = GridPosition::first();
        hm.insert(pos, 1);
        
        loop {
            pos = pos.next();
            let val = pos.adjacents().into_iter().map(|p| hm.get(p).unwrap_or(&0usize)).sum::<usize>();

            if val > n {
                return val;
            }

            hm.insert(pos, val);
        }
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn examples() {
        }
    }
}

